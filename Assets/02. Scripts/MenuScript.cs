﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MenuScript : MonoBehaviour {

	//float timer = 0.0f;
	public float speedRotation = 10.0f; //Al ser public se puede editar en Unity en el Inspector
	/*[Range(2.0f,6.0f)]//En el inspector se ve un slide entre los rangos deseados
	public float testslider = 0.0f;
	[HideInInspector]//No aparece en el Inspector, pero sigue siendo publica para las otras clases
	public float testNOinspector = 0.0f;

	public GameObject spaceship;//public permite vincularlo por el editor de Unity
	GameObject spaceship2; */
	// Use this for initialization
	public GameObject[] lights;
	public GameObject Planet;
	void Start () {	
		foreach (GameObject light in lights)
			if (light.activeSelf) {
				light.SetActive (false);
			}
		InvokeRepeating("LightOff",0.1f,0.75f);
		Planet = GameObject.Find ("Planet");
			/*lights = GameObject.FindGameObjectsWithTag ("SimonSaysLight");
		foreach(GameObject light in lights)
		{
			light.SetActive(false);	
		}*/
			/*	Debug.Log ("MenuScript_Start");
		spaceship2= GameObject.Find ("SpaceShip");//Vinculado a traves de codigo, busqueda poco eficiente, se puede hacer mas rapido con tags
		spaceship2= GameObject.FindGameObjectWithTag ("Tag");
		Debug.LogWarning ("MenuScript_Start");
		Debug.LogError ("MenuScript_Start");
		Invoke ("SayHi", 3.0f); //Ejecuta SayHi al pasar 3 segundos
		InvokeRepeating ("SayHi", 3.0f,2.0f); //Ejecuta SayHi al pasar 3 segundos y se repite cada 2 segundos*/
	}
	
	// Update is called once per frame
	void Update () {
		/*timer += Time.deltaTime;
		if (timer >= 3.0f) {
			Debug.Log ("Game Over");
		}*/
		//Debug.Log ("Time.deltatime="+Time.deltaTime);
		//CancelInvoke ();
		//transform.Rotate (0.0f, Time.deltaTime*speedRotation, 0.0f);//Multiplicar por Time.deltaTime para evitar problemas de diferencia de potencia entre maquinas

			/*int numLight = Random.Range (0, lights.Length);
			lights[numLight].SetActive(true);*/
			//Invoke("LightOff",1.0f); No usar Invoke a no ser que este dentro de condiciones (como al pulsar una tecla)
		Transform PlanetTransf = Planet.GetComponent<Transform> ();
		PlanetTransf.Rotate (0.0f, Time.deltaTime * speedRotation, 0.0f);

		}

	void LightOff()
	{
		foreach (GameObject light in lights)
			if (light.activeSelf) {
				light.SetActive (false);
			}
	//	int numLight = Random.Range (0, lights.Length);
	//	lights[numLight].SetActive(true);
		Invoke("LightOn",0.25f);// Intentar ver luz apagada si le toca 2 veces seguidas
	}

	void LightOn()
	{
		if (lights.Length > 0) {

			int numLight = Random.Range (0, lights.Length);
			//Debug.Log ("NumLight: " + numLight + " Size: " + lights.Length);
			lights [numLight].SetActive (true);
		}
	}








/*	void SayHi()
	{
		Debug.Log ("Hi");
	}*/

}