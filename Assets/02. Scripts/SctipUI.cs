﻿using UnityEngine;
using System.Collections;

public class SctipUI : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Go2Scene(string sceneName)
	{
		Application.LoadLevel (sceneName);
	}

	public void ExitGame()
	{
		#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying=false;
		#elif UNITY_STANDALONE
			Application.Quit();
		#endif
	}
}