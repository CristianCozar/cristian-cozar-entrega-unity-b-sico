﻿using UnityEngine;
using System.Collections;

public class ScriptTestVector : MonoBehaviour {

	public KeyCode key = KeyCode.A; //valor por defecto
	//public GameObject[] cubes;
	public MeshRenderer[] cubesMesh;

	// Use this for initialization
	void Start () {
		//cubes = GameObject.FindGameObjectsWithTag ("mycube");//encuentra todos los objetos con el tag mycube
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (key)) {
			foreach(/*GameObject*/ MeshRenderer cube in cubesMesh/*cubes*/ )
			{
			//	MeshRenderer cubeMeshR = cube.GetComponent<MeshRenderer>();
			//	cubeMeshR.material.color = Color.red;
				cube.material.color = Color.red;
			
			//	cube.gameObject; //Puede entrar al GameObject al que esta vinculado
			}
		}
	
	}
}
